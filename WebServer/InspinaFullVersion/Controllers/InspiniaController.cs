﻿using System.Web.Mvc;

namespace InspinaFullVersion.Controllers
{
    public class InspiniaController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DashBoard_2()
        {
            return View();
        }

        public ActionResult DashBoard_3()
        {
            return View();
        }

        public ActionResult DashBoard_4()
        {
            return View();
        }

        public ActionResult DashBoard_4_1()
        {
            return View();
        }

        public ActionResult DashBoard_5()
        {
            return View();
        }

        public ActionResult x404()
        {
            return View();
        }

        public ActionResult x500()
        {
            return View();
        }

        public ActionResult Agile_Board()
        {
            return View();
        }

        public ActionResult Article()
        {
            return View();
        }

        public ActionResult Badges_Labels()
        {
            return View();
        }

        public ActionResult Basic_Gallery()
        {
            return View();
        }

        public ActionResult Blog()
        {
            return View();
        }

        public ActionResult Buttons()
        {
            return View();
        }

        public ActionResult C3()
        {
            return View();
        }

        public ActionResult Calendar()
        {
            return View();
        }

        public ActionResult Carousel()
        {
            return View();
        }

        public ActionResult Chat_View()
        {
            return View();
        }

        public ActionResult Clients()
        {
            return View();
        }

        public ActionResult Clipboard()
        {
            return View();
        }

        public ActionResult Code_Editor()
        {
            return View();
        }

        public ActionResult Contacts()
        {
            return View();
        }

        public ActionResult Contacts_2()
        {
            return View();
        }

        public ActionResult CSS_Animation()
        {
            return View();
        }

        public ActionResult Diff()
        {
            return View();
        }

        public ActionResult Draggable_Panels()
        {
            return View();
        }

        public ActionResult Ecommerce_Cart()
        {
            return View();
        }

        public ActionResult Ecommerce_Orders()
        {
            return View();
        }

        public ActionResult Ecommerce_Payments()
        {
            return View();
        }

        public ActionResult Ecommerce_Product()
        {
            return View();
        }

        public ActionResult Ecommerce_Product_Detail()
        {
            return View();
        }

        public ActionResult Ecommerce_Product_List()
        {
            return View();
        }

        public ActionResult Ecommerce_Products_Grid()
        {
            return View();
        }

        public ActionResult Email_Template()
        {
            return View();
        }

        public ActionResult Empty_Page()
        {
            return View();
        }

        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult File_Manager()
        {
            return View();
        }

        public ActionResult Forgot_Password()
        {
            return View();
        }


        public ActionResult Form_Advanced()
        {
            return View();
        }

        public ActionResult Form_Basic()
        {
            return View();
        }

        public ActionResult Form_Editors()
        {
            return View();
        }

        public ActionResult Form_File_Upload()
        {
            return View();
        }

        public ActionResult Form_Markdown()
        {
            return View();
        }

        public ActionResult Form_Wizard()
        {
            return View();
        }

        public ActionResult Forum_Main()
        {
            return View();
        }

        public ActionResult Forum_Post()
        {
            return View();
        }

        public ActionResult Full_Height()
        {
            return View();
        }

        public ActionResult Google_Maps()
        {
            return View();
        }

        public ActionResult Graph_Chartist()
        {
            return View();
        }

        public ActionResult Graph_Chartjs()
        {
            return View();
        }

        public ActionResult Graph_Flot()
        {
            return View();
        }

        public ActionResult Graph_Morris()
        {
            return View();
        }

        public ActionResult Graph_Peity()
        {
            return View();
        }

        public ActionResult Graph_Rickshaw()
        {
            return View();
        }

        public ActionResult Graph_Sparkline()
        {
            return View();
        }

        public ActionResult Grid_Options()
        {
            return View();
        }

        public ActionResult i18support()
        {
            return View();
        }

        public ActionResult Icons()
        {
            return View();
        }

        public ActionResult Idle_Timer()
        {
            return View();
        }

        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult Invoice_Print()
        {
            return View();
        }

        public ActionResult Issue_Tracker()
        {
            return View();
        }

        public ActionResult Jq_Grid()
        {
            return View();
        }

        public ActionResult Landing()
        {
            return View();
        }

        public ActionResult Layouts()
        {
            return View();
        }

        public ActionResult Loading_Buttons()
        {
            return View();
        }

        public ActionResult LockScreen()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Login_Two_Columns()
        {
            return View();
        }

        public ActionResult Mail_Compose()
        {
            return View();
        }

        public ActionResult Mail_Detail()
        {
            return View();
        }

        public ActionResult MailBox()
        {
            return View();
        }

        public ActionResult Masonry()
        {
            return View();
        }

        public ActionResult Metrics()
        {
            return View();
        }

        public ActionResult Modal_Window()
        {
            return View();
        }

        public ActionResult Nestable_List()
        {
            return View();
        }

        public ActionResult Notifications()
        {
            return View();
        }

        public ActionResult Off_Canvas_Menu()
        {
            return View();
        }

        public ActionResult Package()
        {
            return View();
        }

        public ActionResult Pin_Board()
        {
            return View();
        }

        public ActionResult Profile_1()
        {
            return View();
        }

        public ActionResult Profile_2()
        {
            return View();
        }

        public ActionResult Project_Detail()
        {
            return View();
        }

        public ActionResult Projects()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Resizeable_Panels()
        {
            return View();
        }

        public ActionResult Search_Results()
        {
            return View();
        }

        public ActionResult Md_Skin()
        {
            return View();
        }

        public ActionResult Skin_Config()
        {
            return View();
        }

        public ActionResult Slick_Carousel()
        {
            return View();
        }

        public ActionResult Social_Feed()
        {
            return View();
        }

        public ActionResult Spinners()
        {
            return View();
        }

        public ActionResult SweetAlert()
        {
            return View();
        }

        public ActionResult Table_Basic()
        {
            return View();
        }

        public ActionResult Table_Data_Tables()
        {
            return View();
        }

        public ActionResult Table_Foo_Table()
        {
            return View();
        }

        public ActionResult Tabs()
        {
            return View();
        }

        public ActionResult Tabs_Panels()
        {
            return View();
        }

        public ActionResult Teams_Board()
        {
            return View();
        }

        public ActionResult Timeline()
        {
            return View();
        }

        public ActionResult Timeline_2()
        {
            return View();
        }

        public ActionResult TinyCon()
        {
            return View();
        }

        public ActionResult Toastr_Notifications()
        {
            return View();
        }


        public ActionResult Tour()
        {
            return View();
        }

        public ActionResult Tree_View()
        {
            return View();
        }

        public ActionResult Truncate()
        {
            return View();
        }

        public ActionResult Typography()
        {
            return View();
        }


        public ActionResult Validation()
        {
            return View();
        }

        public ActionResult Video()
        {
            return View();
        }

        public ActionResult Vote_List()
        {
            return View();
        }

        public ActionResult Widgets()
        {
            return View();
        }
    }
}