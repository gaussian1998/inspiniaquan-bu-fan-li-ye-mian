﻿using System;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Web.Mvc;

namespace InspinaFullVersion.Helpers
{
    public static class HMTLHelperExtensions
    {
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null, string cssClass = null)
        {
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ?
                cssClass : String.Empty;
        }

        public static string InspiniaSelected(this HtmlHelper html, string action)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];

            return action == currentAction ? "active" : String.Empty;
        }

        public static string InspiniaSelected(this HtmlHelper html, string[] actions)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];

            return actions.Contains(currentAction) ? "active" : String.Empty;
        }

        public static string Language(this HtmlHelper html)
        {
            if (string.IsNullOrEmpty(html.ViewBag.Lang))
                return "zh-TW";
            else
                return html.ViewBag.Lang;
        }

        public static string Lookup(this HtmlHelper html, string key)
        {
            var lang = html.Language();
            var rm = new ResourceManager(typeof(Resources.DisplayName));
            var result = rm.GetString(key, CultureInfo.CreateSpecificCulture(lang));

            return string.IsNullOrEmpty(result) ? key : result;
        }

        public static string Error(this HtmlHelper html, TempDataDictionary temp)
        {
            if (temp["Error"] is string)
                return html.Lookup(temp["Error"].ToString());
            else
                return string.Empty;
        }
    }
}
